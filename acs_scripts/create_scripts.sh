#!/bin/bash

_d="$(pwd)"

cat > setup_build.sh << EOF1

# Source aacus
source /opt/aacus/setup.bash
export ROS_PACKAGE_PATH=$_d/src:\$ROS_PACKAGE_PATH
EOF1

chmod +x setup_build.sh

cat > setup_run_internal.sh << EOF2
# Source aacus
source /opt/aacus/setup.bash
export ROS_PACKAGE_PATH=$_d/install:\$ROS_PACKAGE_PATH
export CMAKE_PREFIX_PATH=$_d/install:\$CMAKE_PREFIX_PATH
export PYTHONPATH=$_d/devel/lib/python2.7/dist-packages:\$PYTHONPATH
EOF2

chmod +x setup_run_internal.sh

cat > setup_aacus_install.sh << EOF4
#!/bin/bash
aacus_install(){
  catkin_make install \$@  --cmake-args \
     '-DCMAKE_INSTALL_PREFIX=/opt/aacus/cmu'   '-DBUILD_SHARED_LIBS=0'\
      '-DCMAKE_BUILD_TYPE="Release"'

}

# Source aacus
source /opt/aacus/setup.bash
export ROS_PACKAGE_PATH=$_d/src:\$ROS_PACKAGE_PATH
EOF4

chmod +x setup_aacus_install.sh
